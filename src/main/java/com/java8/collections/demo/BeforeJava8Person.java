package com.java8.collections.demo;

import java.util.Arrays;
import java.util.List;

/**
 * Created by psomani on 4/19/17.
 */
public class BeforeJava8Person {
    public static void main(String[] args) {
        List<Person> personList = Arrays.asList(
                new Person("Payal", 30),
                new Person("Shree",6),
                new Person("Saavi",1),
                new Person("Pankaj", 28));
        Person result = getPersonByName(personList, "Saavi");
        System.out.println(result);
    }

    private static Person getPersonByName(List<Person> personList, String name) {

        Person result = null;
        for(Person personTemp : personList){
            if(name.equals(personTemp.getName())){
                result = personTemp;
            }
        }
    return result;
    }
}
