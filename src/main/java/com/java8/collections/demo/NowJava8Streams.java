package com.java8.collections.demo;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by psomani on 4/19/17.
 */
public class NowJava8Streams {
    public static void main(String[] args) {
        List<String> lines = Arrays.asList("Spring", "NodeJS","Pankaj", "Payal");
        List<String> result = lines.stream()
                .filter(line -> !"Pankaj".equals(line))
                //.peek(  "This is Peek")
                .collect(Collectors.toList());
        result.forEach(System.out::println);
    }
}
