package com.java8.collections.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by psomani on 4/19/17.
 */
public class BeforeJava8Strams {

    public static void main(String[] args){
        List<String> lines = Arrays.asList("Spring", "NodeJS","Pankaj", "Payal");
        List<String> result = getFilterOutPut(lines, "Pankaj");
        for(String tempStr : result){
            System.out.println(tempStr);
        }

    }

    private static List<String> getFilterOutPut(List<String> lines, String filter) {
        List<String> result = new ArrayList<String>();
        for(String line : lines){
            if(!"Pankaj".equals(line)){
                result.add(line);
            }
        }
        return result;
    }
}
