package com.java8.collections.demo;

import java.util.Arrays;
import java.util.List;

/**
 * Created by psomani on 4/19/17.
 */
public class NowJava8Person {
    public static void main(String[] args) {
        List<Person> personList = Arrays.asList(
                new Person("Payal", 30),
                new Person("Shree",6),
                new Person("Saavi",1),
                new Person("Pankaj", 28));

        Person result1 = personList.stream()
                .filter(r1 -> "Pankaj".equals(r1.getName()))
                .findAny()
                .orElse(null);

        System.out.println(result1);

        Person result2 = personList.stream()
                .filter(r2-> "pankaj".equals(r2.getName()))
                .findAny()
                //.orElseThrow(() -> new BadRequest("required query parameter TYPE is missing"))
                .orElse(null);

        System.out.println(result2);

    }
}
